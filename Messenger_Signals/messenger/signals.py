from django.dispatch import receiver
from django.db.models.signals import post_save
from .models import ActivityLog, Chat


@receiver(post_save, sender=Chat)
def message_activity(sender, instance, created, **kwargs):
    action = 'Message sent' if created else 'Message updated'
    ActivityLog.objects.create(message_sent=instance, action=action)
