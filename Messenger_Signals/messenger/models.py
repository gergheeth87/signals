from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

User = get_user_model()


class Chat(models.Model):
    objects = None
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    recipient_of_message = models.TextField(max_length=15, default='')
    message_text = models.TextField(max_length=100)
    published_date = models.DateTimeField(default=timezone.now)

    def can_view_chat(self, user):
        return user.is_authenticated

    class Meta:
        permissions = [
            ('can_view_chat', 'User can view chat'),
        ]


class AddChatUsers(models.Model):
    objects = None
    add_active_users = models.BooleanField(default=False)


class ActivityLog(models.Model):
    message_sent = models.ForeignKey(Chat, on_delete=models.DO_NOTHING, blank=True, null=True)
    action = models.CharField(max_length=500)
    timestamp = models.DateTimeField(auto_now_add=True)
