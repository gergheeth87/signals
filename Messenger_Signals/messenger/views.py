from django.utils import timezone
from django.shortcuts import render, redirect, get_list_or_404
from django.views import View
from .forms import ChatForm, LoginForm, AddChatUsersForm
from .models import Chat, User, AddChatUsers
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.views.generic.edit import UpdateView, DeleteView, FormView, CreateView
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from .mixins import AuthorAndElapsedTimeRequiredMixin, ActiveUserRequiredMixin, RedirectIfSuperuserdMixin
from django.contrib import messages


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('http://127.0.0.1:8000/connect_to_chat/')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'messenger/user_login.html', {'form': form})


@permission_required('messenger.can_view_chat')
def add_user_perm(request):
    pr_chat = get_list_or_404
    if request.method == "POST":
        form = AddChatUsersForm(request.POST)
        if form.is_valid():
            form.instance.post = pr_chat
            form.save()
    else:
        form = AddChatUsersForm()
    if request.method == "POST":
        selected_users = request.POST.getlist('users')
        content_type = ContentType.objects.get_for_model(Chat)
        can_view_chat_permission = Permission.objects.filter(
            content_type=content_type,
            codename='can_view_chat'
        ).first()

        for user_id in selected_users:
            allowed_user = User.objects.filter(id=user_id).first()
            if allowed_user:
                allowed_user.user_permissions.add(can_view_chat_permission)

        add_active_user(request)
        restricted_users = User.objects.all().exclude(id__in=selected_users)

        for user in restricted_users:
            user.user_permissions.remove(can_view_chat_permission)

        add_active_user(request)

        return redirect('add_user_perm')

    users = User.objects.all()

    return render(request, 'messenger/add_user_perm.html', {'users': users, 'form': form})


@permission_required('messenger.can_view_chat')
def add_active_user(request):
    pr_chat = get_list_or_404
    if request.method == "POST":
        form = AddChatUsersForm(request.POST)
        if form.is_valid():
            form.instance.post = pr_chat
            form.save()
    else:
        form = AddChatUsersForm()

    content_type = ContentType.objects.get_for_model(Chat)
    can_view_chat_permission = Permission.objects.filter(
        content_type=content_type,
        codename='can_view_chat'
    ).first()
    users = User.objects.all()
    added_active_users = AddChatUsers.objects.last()

    if added_active_users.add_active_users:
        for user in users:
            if user.is_active:
                user.user_permissions.add(can_view_chat_permission)

    users = User.objects.all()

    return render(request, 'messenger/add_user_perm.html', {'form': form, 'users': users})


@permission_required('messenger.can_view_chat')
def chat(request):
    pr_chat = get_list_or_404
    if request.method == "POST":
        form = ChatForm(request.POST)
        if form.is_valid():
            form.instance.author = request.user
            form.instance.post = pr_chat
            form.save()
            last_chat_message = Chat.objects.last()
            users = User.objects.all()
            for user in users:
                if last_chat_message.recipient_of_message == user.username and user.is_superuser:
                    messages.success(request, 'You have successfully sent a message to superuser')
            return redirect('http://127.0.0.1:8000/chat/')
    else:
        form = ChatForm()
    chat_message = Chat.objects.all()
    last_chat_message = Chat.objects.last()
    return render(request, 'messenger/chat.html', {'form': form,
                                                   'chat_message': chat_message,
                                                   'last_chat_message': last_chat_message})


class ConnectToChat(ActiveUserRequiredMixin, RedirectIfSuperuserdMixin, View):
    redirect_to = 'http://127.0.0.1:8000/add_user_perm/'
    template_name = 'messenger/connect_to_chat.html'

    def get(self, request):
        return render(request, self.template_name)


class ChatUpdateView(AuthorAndElapsedTimeRequiredMixin, UpdateView):
    allowed_days = 1
    request_date = timezone.now()

    model = Chat
    fields = ['message_text']
    template_name_suffix = '_edit_form'
    success_url = 'http://127.0.0.1:8000/chat/'


class ChatDeleteView(AuthorAndElapsedTimeRequiredMixin, DeleteView):
    allowed_days = 1
    request_date = timezone.now()

    model = Chat
    fields = ['message_text']
    template_name_suffix = '_confirm_delete'
    success_url = 'http://127.0.0.1:8000/chat/'
